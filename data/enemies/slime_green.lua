-- Lua script of enemy slime_green.
-- This script is executed every time an enemy with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

-- AI varibles:
local aware_of_hero = false

-- Event called when the enemy is initialized.
function enemy:on_created()
    sprite = self:create_sprite("enemies/" .. self:get_breed())
    enemy:set_life(2)
    enemy:set_damage(1)
end

local function start_target_movement(self)
    movement = sol.movement.create("target")
    movement:set_target(hero)
    movement:set_speed(36)
    movement:start(self)
end

function enemy:on_restarted()
    if aware_of_hero then
        start_target_movement(self)
    else
        movement = sol.movement.create("random")
        movement:set_speed(24)
        movement:start(self)
    end
end

function enemy:on_update()
    if not aware_of_hero and self:get_distance(hero) < 80 then
        aware_of_hero = true
        movement:stop()
        start_target_movement(self)
    end
end
