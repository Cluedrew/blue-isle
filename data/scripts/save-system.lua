-- Manages multiple saves.

local save_system = {}

-- This is the "create" function except the module isn't instance based.
function save_system.setup(file_name)
    if not sol.file.exists(file_name) then

    end
end

function save_system.start_game(file_name)
    local game = sol.game.load(file_name)
    if not sol.game.exists(file_name) then
        local on_game_created = save_system.on_game_created
        if on_game_created then
            on_game_created(game, file_name)
        end
    end
    game:start()
    return game
end

return save_system
