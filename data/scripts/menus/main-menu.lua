-- The main menu for when you first enter the game.

-- Returns a singlar menu object.

local main_menu = {}

local a_blue_colour = {95, 110, 225}

local cursor_index = 1
local options = {"start", "quit"}
local num_options = #options
local option_actions = {
    -- This one is probably not final.
    start = function(self)
        sol.menu.stop(self)
    end,
    quit = function(self)
        sol.main.exit()
    end,
}

function main_menu:on_draw(dst_surface)
    local dst_w, dst_h = dst_surface:get_size()

    -- Just a solid blue background for now.
    dst_surface:fill_color(a_blue_colour)
    
    for i, option in ipairs(options) do
        -- The highlight isn't working, will need to find a new font.
        local colour
        if i == cursor_index then
            colour = {255, 255, 255}
            -- Work-around highlight.
            option = "> " .. option .. " <"
        else
            colour = {128, 128, 128}
        end

        local label = sol.text_surface.create{
            text = option,
            color = colour,
        }

        local x, y = label:get_size()
        x = (dst_w - x) / 2
        y = dst_h - (num_options - i + 2) * 15
        
        label:draw(dst_surface, x, y)
    end
end

-- Handles mapped inputs.
function main_menu:on_menu_command(command)
    if "up" == command then
        if 1 == cursor_index then
            cursor_index = num_options
        else
            cursor_index = cursor_index - 1
        end
    elseif "down" == command then
        if num_options == cursor_index then
            cursor_index = 1
        else
            cursor_index = cursor_index + 1
        end
    elseif "select" == command then
        option_actions[options[cursor_index]](self)
    else
        return false
    end
    return true
end

-- Maybe this bit should go into a metatable or utilty script.
-- I don't have a way to configure them yet.
-- key: command
local menu_commands = {
    up = "up",
    down = "down",
    left = "left",
    right = "right",
    space = "select",
}

-- Just does some input mapping.
function main_menu:on_key_pressed(key, modifiers)
    local command = menu_commands[key]
    if command then
        return self:on_menu_command(command)
    end
    
end

return main_menu
