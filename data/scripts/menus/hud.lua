-- The display used while normal game-play is ongoing.

-- This is a singluar menu and does not have multiple instances.

local hud = {}

local full_heart = sol.sprite.create"hud/hearts"
full_heart:set_animation"red_heart"
full_heart:set_direction(3)

local partial_heart = sol.sprite.create"hud/hearts"
partial_heart:set_animation"red_heart"

local empty_heart = sol.sprite.create"hud/hearts"
empty_heart:set_animation"empty_heart"

local flashing = false

function hud:on_draw(dst_surface)
    local game = sol.main.get_game()

    self:draw_health(dst_surface, game)
    self:draw_money(dst_surface, game)
end

function hud:draw_health(dst_surface, game)
    local cur_health = game:get_life()
    local max_health = game:get_max_life()

    local should_flash = (cur_health <= max_health / 3)
    if should_flash and not flashing then
        empty_heart:set_animation"flashing_heart"
    elseif flashing and not should_flash then
        empty_heart:set_animation"empty_heart"
    end
    flashing = should_flash

    for i = 1, (max_health / 4) do
        local x = 10 + (10 * i)
        local y = 10
        empty_heart:draw(dst_surface, x, y)
        if 4 <= cur_health then
            full_heart:draw(dst_surface, x, y)
            cur_health = cur_health - 4
        elseif 0 < cur_health then
            partial_heart:set_direction(cur_health - 1)
            partial_heart:draw(dst_surface, x, y)
            cur_health = 0
        end
    end
end

function hud:draw_money(dst_surface, game)
    local width, height = dst_surface:get_size()
    local cur_money = game:get_money()

    local label = sol.text_surface.create{
        text = tostring(cur_money),
    }
    label:draw(dst_surface, 15, 20)
end

function hud:draw_equipment(dst_surface, game)
    local item1 = game:get_item_assigned(1)
    local item2 = game:get_item_assigned(2)
    -- TODO (once I have items)
end

-- TODO draw_interact

return hud
