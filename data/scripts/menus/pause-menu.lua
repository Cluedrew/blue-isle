-- Main pause menu.

-- This is a singluar menu and does not have multiple instances.

local pause_menu = {}

function pause_menu:on_draw(dst_surface)
    local dst_w, dst_h = dst_surface:get_size()
    local tint = sol.surface.create(dst_w, dst_h)
    tint:fill_color({128, 128, 128})
    tint:set_blend_mode("multiply")
    tint:draw(dst_surface)
    
    local label = sol.text_surface.create{
        text = "Paused",
    }
    local label_w, label_h = label:get_size()
    label:draw(dst_surface, (dst_w - label_w) / 2, (dst_h - label_w) / 2)
end

return pause_menu
