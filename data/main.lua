-- Main Lua script of the quest.
-- See the Lua API! http://www.solarus-games.org/doc/latest

-- Global Configuration:
require("scripts/dialogs/visual_novel_manager")

-- Modules:
local save_system = require"scripts/save-system"
local solarus_logo = require"scripts/menus/solarus_logo"
local main_menu = require"scripts/menus/main-menu"
local pause_menu = require"scripts/menus/pause-menu"
local hud = require"scripts/menus/hud"

local game_mt = sol.main.get_metatable"game"

-- Events:
function save_system.on_game_created(game)
    game:set_starting_location"roost_town"
    --game:set_starting_location"lab_entrance"

    game:set_max_life(12)
    game:set_life(12)
    game:set_max_money(100)
    game:set_money(0)
    game:set_ability("lift", 1)
    game:set_ability("sword", 1)
end

function sol.main:on_started()
    sol.menu.start(sol.main, solarus_logo)
end

function solarus_logo:on_finished()
    sol.menu.start(sol.main, main_menu)
end
    
function main_menu:on_finished()
    local game = save_system.start_game"save1.dat"
end

-- This causes a problem if you start it from ZeroBrane.
game_mt:register_event('on_started', function(self)
    sol.menu.start(self, hud)
end)

function game_mt:on_command_pressed(command)
    if "pause" == command then
        if self:is_suspended() then
            self:set_suspended(false)
            sol.menu.stop(pause_menu)
        else
            self:set_suspended(true)
            sol.menu.start(self, pause_menu, true)
        end
        return true
    end
end
